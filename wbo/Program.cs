﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WBO.Opcodes;

namespace WBO
{
    class Program
    {
        static void PrintUsage()
        {
            Console.WriteLine("usage: WBO <input>");
            Console.WriteLine("  - converts a .obj file to a .wbo file");
        }

        public static readonly char[] TrimCharacters = { ' ', '\t' };

        private static string[] SplitLine(string line)
        {
            return line.Split(TrimCharacters, StringSplitOptions.RemoveEmptyEntries);
        }

        private static string[] GetInput(string[] line)
        {
            var newLength = line.Length - 1;

            if (newLength == 0)
                return null;

            var input = new string[newLength];

            Array.Copy(line, 1, input, 0, newLength);

            return input;
        }

        static void ProcessObj(string filename)
        {
            using (var obj = new StreamReader(filename))
            {
                var str = "";
                var line = 0;

                var opcodes = new List<IOpcode>();

                while ((str = obj.ReadLine()) != null)
                {
                    ++line;

                    if (str == "" || str.StartsWith("#"))
                        continue;

                    var split = SplitLine(str);
                    var key = split[0];

                    var op = OpcodeFactory.Create(key);

                    if (op != null)
                    {
                        var input = GetInput(split);
                        op.ProcessASCII(input);
                    }
                    else
                    {
                        Console.WriteLine("*** Unknown opcode '{0}' on line {1} ***", key, line);
                    }

                    opcodes.Add(op);
                }

                Console.WriteLine("Operation completed.");
            }
        }

        static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                Console.Error.WriteLine("error: too many arguments");
            }
            else if (args.Length == 1)
            {
                var filename = args[0];

                if (File.Exists(filename) && Path.GetExtension(filename) == ".obj")
                {
                    ProcessObj(filename);
                }
                else
                {
                    Console.Error.WriteLine("error: invalid .obj filename");
                }
            }
            else
            {
                PrintUsage();
            }
        }
    }
}
