﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    public interface IOpcode
    {
        byte Flags { get; set; }

        void ProcessASCII(string[] input);
        void ProcessBinary(WBOReader input);

        void CompileASCII(StreamWriter output);
        void CompileBinary(WBOWriter output);
    }
}
