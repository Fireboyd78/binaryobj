﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    public enum FaceRefType : byte
    {
        RT_8    = 0,
        RT_16   = 1,
        RT_32   = 2
    }
}
