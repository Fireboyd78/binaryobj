﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    public enum FaceType : byte
    {
        Vertex              = 0,    // "v"
        VertexAndTexture    = 1,    // "v/v"
        VertexAndNormal     = 2,    // "v//v"
        AllVertexComponents = 3     // "v/v/v"
    }
}
