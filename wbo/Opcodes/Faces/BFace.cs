﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.Face, "f")]
    public class BFace : BOpcode
    {
        private static readonly char[] FaceSeparator = { '/' };

        protected const int MASK_REF_TYPE   = 0x03;
        protected const int MASK_FACE_TYPE  = 0x0C;
        
        public FaceRefType RefType
        {
            get { return (FaceRefType)GetFlag(MASK_REF_TYPE, 0); }
            set { SetFlag(MASK_REF_TYPE, 0, (byte)value); }
        }

        public FaceType FaceType
        {
            get { return (FaceType)GetFlag(MASK_FACE_TYPE, 2); }
            set { SetFlag(MASK_FACE_TYPE, 2, (byte)value); }
        }

        public int RefSize
        {
            get { return (8 << (byte)RefType); }
        }

        public int RefStride
        {
            get
            {
                switch (FaceType)
                {
                case FaceType.Vertex:
                    return 1;
                case FaceType.VertexAndTexture:
                case FaceType.VertexAndNormal:
                    return 2;
                case FaceType.AllVertexComponents:
                    return 3;
                default:
                    throw new InvalidOperationException("Unknown stride");
                }
            }
        }

        public List<Int32> FaceRefs { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            FaceRefs = new List<Int32>();

            for (int t = 0; t < input.Length; t++)
            {
                var ind = input[t].Split(FaceSeparator, StringSplitOptions.None);

                switch (ind.Length)
                {
                case 1:
                    FaceType = FaceType.Vertex;
                    break;
                case 2:
                    FaceType = FaceType.VertexAndTexture;
                    break;
                case 3:
                    FaceType = (String.IsNullOrEmpty(ind[1])) ? FaceType.VertexAndNormal : FaceType.AllVertexComponents;
                    break;
                }

                int i0, i1, i2;

                FaceRefs.Add((int.TryParse(ind[0], out i0)) ? i0 - 1 : -1);
                FaceRefs.Add((int.TryParse(ind[1], out i1)) ? i1 - 1 : -1);

                if (ind.Length == 3)
                    FaceRefs.Add((int.TryParse(ind[2], out i2)) ? i2 - 1 : -1);
            }
        }
    }
}
