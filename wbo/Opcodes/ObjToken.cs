﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO
{
    public enum ObjToken : byte
    {
        /* ==== Vertex data ==== */
        Vertex,                     // v
        VertexNormal,               // vn
        VertexTexture,              // vt
        ParameterSpaceVertex,       // vp
        Degree,                     // deg
        BasisMatrix,                // bmat
        StepSize,                   // step

        /* ==== Elements ==== */
        Point,                      // p
        Line,                       // l
        Face,                       // f
        Curve,                      // curv
        Curve2D,                    // curv2
        Surface,                    // surf
        
        /* ==== Free-form curve/surface body statements ==== */
        ParameterValue,             // parm
        OuterTrimmingLoop,          // trim
        InnerTrimmingLoop,          // hole
        SpecialCurve,               // scrv
        SpecialPoint,               // sp
        EndStatement,               // end
        
        /* ==== Connectivity between free-form surfaces ==== */
        Connect,                    // con
        
        /* ==== Grouping ==== */
        GroupName,                  // g
        SmoothingGroup,             // s
        MergingGroup,               // mg
        ObjectName,                 // o
        
        /* ==== Display/render attributes ==== */
        Bevel,                      // bevel
        ColorInterpolation,         // c_interp
        DissolveInterpolation,      // d_interp
        LevelOfDetail,              // lod
        MaterialName,               // usemtl
        MaterialLibrary,            // mtllib
        ShadowCastingObject,        // shadow_obj
        RayTracing,                 // trace_obj
        CurveApproxTechnique,       // ctech
        SurfApproxTechnique         // stech
    }
}
