﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.MaterialLibrary, "mtllib")]
    public class BMaterialLib : BOpcode
    {
        public List<string> FileNames { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            var nFiles = input.Length;

            FileNames = new List<string>(nFiles);

            for (int i = 0; i < nFiles; i++)
                FileNames.Add(input[i]);
        }
    }
}
