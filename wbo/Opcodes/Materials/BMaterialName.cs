﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.MaterialName, "usemtl")]
    public class BMaterialName : BOpcode
    {
        public string Material { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            Material = input[0];
        }
    }
}
