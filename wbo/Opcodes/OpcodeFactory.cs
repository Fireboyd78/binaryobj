﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WBO.Opcodes
{
    /// <summary>
    /// A class encapsulating the creation of opcodes.
    /// </summary>
    public static class OpcodeFactory
    {
        static OpcodeFactory()
        {
            // register default opcodes
            RegisterOpcodesInAssembly(typeof(OpcodeFactory).Assembly);
        }
        
        private static Type[] _opcodes;
        private static IDictionary<string, Type> _opcodeLookup;

        internal static IDictionary<string, Type> OpcodeLookup
        {
            get
            {
                if (_opcodeLookup == null)
                    _opcodeLookup = new Dictionary<string, Type>();

                return _opcodeLookup;
            }
        }

        internal static Type[] Opcodes
        {
            get
            {
                if (_opcodes == null)
                    _opcodes = new Type[256];

                return _opcodes;
            }
        }

        public static IOpcode Create(byte token)
        {
            if (!IsOpcodeRegistered(token))
                return null;

            return (IOpcode)Activator.CreateInstance(Opcodes[token]);
        }

        public static IOpcode Create(string name)
        {
            if (!IsOpcodeRegistered(name))
                return null;

            return (IOpcode)Activator.CreateInstance(OpcodeLookup[name]);
        }
        
        internal static bool IsOpcodeRegistered(byte token)
        {
            return (Opcodes[token] != null);
        }

        internal static bool IsOpcodeRegistered(string name)
        {
            return OpcodeLookup.ContainsKey(name);
        }

        internal static bool IsOpcodeRegistered(byte token, string name)
        {
            return (IsOpcodeRegistered(name) && IsOpcodeRegistered(name));
        }

        internal static void RegisterOpcode(byte token, string name, Type type)
        {
            if (IsOpcodeRegistered(token))
                throw new InvalidOperationException("Cannot register an opcode with a token that's already been consumed.");
            if (IsOpcodeRegistered(name))
                throw new InvalidOperationException("Cannot register an opcode with a name that's already been consumed.");

            Opcodes[token] = type;
            OpcodeLookup.Add(name, type);
        }
        
        /// <summary>
        /// Registers all of the opcodes in the specified assembly that have an <see cref="OpcodeAttribute"/> attached to them.
        /// </summary>
        /// <param name="assembly">The assembly containing the types to iterate through.</param>
        public static void RegisterOpcodesInAssembly(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsClass && typeof(IOpcode).IsAssignableFrom(type))
                {
                    var attrs = type.GetCustomAttributes(typeof(OpcodeAttribute), false);

                    if (attrs.Length == 0)
                        continue;

                    var attr = attrs[0] as OpcodeAttribute;
                    
                    RegisterOpcode(attr.Token, attr.Name, type);
                }
            }
        }
    }
}
