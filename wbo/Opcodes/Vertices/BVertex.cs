﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.Vertex, "v")]
    public class BVertex : BOpcode
    {
        protected const int FLAG_DOUBLE_PRECISION   = 0;
        protected const int FLAG_VERTEX_COLOR       = 1;
        
        public bool DoublePrecision
        {
            get { return GetBitFlag(FLAG_DOUBLE_PRECISION); }
            set { SetBitFlag(FLAG_DOUBLE_PRECISION, value); }
        }

        public bool HasVertexColor
        {
            get { return GetBitFlag(FLAG_VERTEX_COLOR); }
            set { SetBitFlag(FLAG_VERTEX_COLOR, value); }
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public int Color { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            X = double.Parse(input[0]);
            Y = double.Parse(input[1]);
            Z = double.Parse(input[2]);
        }
    }
}
