﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.VertexTexture, "vt")]
    public class BVertexTexture : BOpcode
    {
        protected const int FLAG_DOUBLE_PRECISION   = 0;
        protected const int FLAG_COMPRESSED         = 1;
        protected const int FLAG_W_COMPONENT        = 2;
        
        public bool DoublePrecision
        {
            get { return GetBitFlag(FLAG_DOUBLE_PRECISION); }
            set { SetBitFlag(FLAG_DOUBLE_PRECISION, value); }
        }

        public bool Compressed
        {
            get { return GetBitFlag(FLAG_COMPRESSED); }
            set { SetBitFlag(FLAG_COMPRESSED, value); }
        }

        public bool HasWComponent
        {
            get { return GetBitFlag(FLAG_W_COMPONENT); }
            set { SetBitFlag(FLAG_W_COMPONENT, value); }
        }

        public double U { get; set; }
        public double V { get; set; }
        public double W { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            U = double.Parse(input[0]);
            V = double.Parse(input[1]);

            if (input.Length > 2)
            {
                W = double.Parse(input[2]);
                HasWComponent = true;
            }
        }
    }
}
