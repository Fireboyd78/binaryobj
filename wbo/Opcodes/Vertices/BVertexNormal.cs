﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.VertexNormal, "vn")]
    public class BVertexNormal : BOpcode
    {
        protected const int FLAG_DOUBLE_PRECISION   = 0;
        protected const int FLAG_COMPRESSED         = 1;

        protected const int FLAG_NEG_X              = 2;
        protected const int FLAG_NEG_Y              = 3;
        protected const int FLAG_NEG_Z              = 4;
        
        public bool DoublePrecision
        {
            get { return GetBitFlag(FLAG_DOUBLE_PRECISION); }
            set { SetBitFlag(FLAG_DOUBLE_PRECISION, value); }
        }

        public bool Compressed
        {
            get { return GetBitFlag(FLAG_COMPRESSED); }
            set { SetBitFlag(FLAG_COMPRESSED, value); }
        }

        public bool NegX
        {
            get { return GetBitFlag(FLAG_NEG_X); }
            set { SetBitFlag(FLAG_NEG_X, value); }
        }

        public bool NegY
        {
            get { return GetBitFlag(FLAG_NEG_Y); }
            set { SetBitFlag(FLAG_NEG_Y, value); }
        }

        public bool NegZ
        {
            get { return GetBitFlag(FLAG_NEG_Z); }
            set { SetBitFlag(FLAG_NEG_Z, value); }
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            X = double.Parse(input[0]);
            Y = double.Parse(input[1]);
            Z = double.Parse(input[2]);
        }
    }
}
