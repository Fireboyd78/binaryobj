﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.GroupName, "g")]
    public class BGroupName : BOpcode
    {
        public string Name { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            Name = input[0];
        }
    }
}
