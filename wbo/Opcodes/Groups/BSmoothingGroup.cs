﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    [Opcode(ObjToken.SmoothingGroup, "s")]
    public class BSmoothingGroup : BOpcode
    {
        protected const int FLAG_SOFF_MODE  = 0;
        protected const int FLAG_16BIT      = 1;
        
        public bool SOffMode
        {
            get { return GetBitFlag(FLAG_SOFF_MODE); }
            set { SetBitFlag(FLAG_SOFF_MODE, value); }
        }

        public bool Is16Bit
        {
            get { return GetBitFlag(FLAG_16BIT); }
            set { SetBitFlag(FLAG_16BIT, value); }
        }

        public int SmoothingGroup { get; set; }

        protected override void ProcessASCII(string[] input)
        {
            var val = input[0];

            if (val.ToLower() == "off")
            {
                SOffMode = true;
            }
            else
            {
                SmoothingGroup = Int32.Parse(val);

                if (SmoothingGroup > 255)
                    Is16Bit = true;
            }
        }
    }
}
