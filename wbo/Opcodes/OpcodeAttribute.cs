﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBO.Opcodes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class OpcodeAttribute : Attribute
    {
        public byte Token { get; private set; }
        public string Name { get; private set; }
        
        public OpcodeAttribute(byte token, string name)
        {
            Name = name;
            Token = token;
        }

        public OpcodeAttribute(ObjToken token, string name)
            : this((byte)token, name)
        {
        }
    }
}
