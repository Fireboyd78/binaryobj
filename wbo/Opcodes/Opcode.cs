﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WBO.Opcodes
{
    public abstract class BOpcode : IOpcode
    {
        #region IOpcode
        void IOpcode.ProcessASCII(string[] input)
        {
            this.ProcessASCII(input);
        }

        void IOpcode.ProcessBinary(WBOReader input)
        {
            this.ProcessBinary(input);
        }

        void IOpcode.CompileASCII(StreamWriter output)
        {
            this.CompileASCII(output);
        }

        void IOpcode.CompileBinary(WBOWriter output)
        {
            this.CompileBinary(output);
        }
        #endregion
        
        public byte Flags { get; set; }

        protected int GetFlag(int mask, int pos)
        {
            return (Flags & mask) >> pos;
        }

        protected void SetFlag(int mask, int pos, int value)
        {
            Flags = (byte)((Flags & ~mask) | (value << pos));
        }

        protected bool GetBitFlag(int bit)
        {
            return (Flags & (1 << bit)) == 1;
        }

        protected void SetBitFlag(int bit, bool enabled)
        {
            if (enabled)
            {
                Flags |= (byte)((1 << bit) & 0xFF);
            }
            else
            {
                Flags &= (byte)(~(1 << bit) & 0xFF);
            }
        }

        protected virtual void ProcessASCII(string[] input)
        {
            throw new NotImplementedException();
        }

        protected virtual void ProcessBinary(WBOReader input)
        {
            throw new NotImplementedException();
        }

        protected virtual void CompileASCII(StreamWriter output)
        {
            throw new NotImplementedException();
        }

        protected virtual void CompileBinary(WBOWriter output)
        {
            throw new NotImplementedException();
        }
    }
}
